


## Meeting on:
August 15th, 2015 
## Time
6.30PM 

## Place
##Workspace 11

## Team members:
Anjali Shenoy, Saumya Rawat, Remya Keerthana, Sreeja Kamishetty, Dhruv Sapra

## Agenda:
General meeting regarding to discuss role requirements for project, doubts relating to assignments

### Minutes
### Discussion
	*Decided as a group that everyone will take the role of testers and programmers

	*Appointed roles to members 
		-Artist: Anjali
		-Designer: Saumya
		-Programmer: Sreeja
		-Level designer: Remya
		-Sound engineer: Dhruv
	*Discussed on various softwares required for different roles
		-Designer: Photoshop
		-Sound: SVP, Adobe after-effects
		-Artist: 3DS Max
	*Doubts regarding assignments




### Action Points
	-Decided on what to discuss with client



## Date of the next meeting
	-if required