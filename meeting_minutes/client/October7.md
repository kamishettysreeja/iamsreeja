###Date-October 7, '15
##Attendes
* Saumya Rawat
* Anjali Shenoy
* Sreeja Kamishetty
* Ramya K

##Agenda
Discussion on the look of the Main Menu and the first scene.
###Discussion
* We met with the Design team Mr. Kalpesh Khatri and Ms. Kavyani.
* The look of the first frame of the game, the cutscenes and their transitions, the loading of the game were closely discussed on.
* How we are going to implement the concept of 15 lessons that we want to teach the users through the game was also finalised. We will be putting up one randomly chosen message at the end of the levels as the player moves to the checkout counter

###Action Point
* As per the request of the client, the font and its colour in the main menu, the choose character layout will be changed.
* Proper framing and design of the messages to be implemented.
##Next Meeting
October 9, '15
Friday.
