## Date and time
September 24th Thursday

## Attendees
Saumya Rawat, Anjali Shenoy, Sreeja Kamishetty, Ramya

## Agenda
Discussion on the final design of characters.

## Minutes
### Discussion
* Final design of characters
    * We proposed that the characters can be made to look younger to suit the game more.
    * Suggestions for dress designs for the female character.
        * Instead of a dress, casual shirts and tank tops paired with jeans.
* Agenda for 29th September IBM conference.
    * Basic look of the menu.
    * Screenshot of the menu to be sent.
* Agenda for R1.
    * Main Menu.
    * Player should be able to choose character.
    * Player should be able to start game play and move the trolley around.

## Date of the next meeting
*(not identified)*
